var open_button = document.querySelector("#show");

function show_alert(text){
    var alert_template = document.querySelector("#alert-template");
    var banner = alert_template.content.querySelector("#alert").cloneNode(true);
    var block  = alert_template.content.querySelector("#block").cloneNode();
    var message = banner.querySelector(".banner-text");
    message.innerHTML = text;
    var added_block = document.body.appendChild(block);
    var added_banner = document.body.appendChild(banner);
    var button = banner.querySelector(".banner-button");
    button.onclick = function(e){
	document.body.removeChild(added_block);
	document.body.removeChild(added_banner);
    }}

function CountBanner(){
    var count_banner_template = document.querySelector("#count-banner");
    var banner = count_banner_template.content.querySelector("#banner").cloneNode(true);
    var block  = count_banner_template.content.querySelector("#block").cloneNode();
    this.set_text = function(text){
	banner.innerHTML = text;
    }

    var added_block = document.body.appendChild(block);
    var added_banner = document.body.appendChild(banner);

    this.stop=function(){
	document.body.removeChild(added_block);
	document.body.removeChild(added_banner);
    }
}

function Banner(){
    //Creates a banner object
    var banner_template = document.querySelector("#banner-template");
    var banner = banner_template.content.querySelector("#alert").cloneNode(true);
    var block  = banner_template.content.querySelector("#block").cloneNode();
    var message = banner.querySelector(".banner-text");
    
    this.set_text = function(text){
	message.innerHTML = text;
    }
    var added_block = document.body.appendChild(block);
    var added_banner = document.body.appendChild(banner);

    this.stop=function(){
	document.body.removeChild(added_block);
	document.body.removeChild(added_banner);
    }    
}

function TwoButtonsBanner(text,button1_text,button2_text){
    var banner_template = document.querySelector("#two-buttons-banner");
    var banner = banner_template.content.querySelector("#banner").cloneNode(true);
    var block  = banner_template.content.querySelector("#block").cloneNode();
    var message = banner.querySelector(".banner-text");
    this.button1 = banner.querySelector("#button1");
    this.button2 = banner.querySelector("#button2");
    this.button1.innerHTML = button1_text;
    this.button2.innerHTML = button2_text;
    message.innerHTML = text;

    var added_block = document.body.appendChild(block);
    var added_banner = document.body.appendChild(banner);
    
    this.stop=function(){
	document.body.removeChild(added_block);
	document.body.removeChild(added_banner);
    }
    
}
