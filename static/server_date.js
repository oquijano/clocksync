"use strict";

class serverDate{
    constructor(ws_date_path,update_interval=1000*60){
	// ws_date_path - websocket url for synchonizing with the server date
	// update_interval - How often to synchronize in miliseconds (default = 1min)
	this.update_interval = update_interval;
	this.offset = null;
	this.precission =  Infinity ;
	this.socket = new WebSocket(ws_date_path);
	let this_aux = this;
	this.socket.onmessage = function(e){
	    let current_date = new Date().getTime();
	    let dates = JSON.parse(e.data);
	    let current_precission = current_date - dates[0];
	    if(current_precission < this_aux.precission){
		this_aux.precission = current_precission;
		this_aux.offset = (current_date + dates[0])/2 - dates[1];		
	    }
	}
	this.socket.onopen = function(e){
	    this_aux.offset_update();
	}
	setInterval(()=>{this_aux.offset_update()},
		    update_interval);
    }

    offset_update(){
	this.precission = Infinity;
	var this_aux=this;
	for (let i=0;i<10;i++){
	    setTimeout(() => {this_aux.socket.send(JSON.stringify(new Date().getTime()))},
		       i*20);
	}	
    }

    
    
    get(){
	return new Date().getTime() - this.offset;
    }
}
